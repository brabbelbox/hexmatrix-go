module hexmatrix-go

go 1.22

require (
	github.com/gorilla/websocket v1.5.1
	github.com/xthexder/go-jack v0.0.0-20220805234212-bc8604043aba
)

require golang.org/x/net v0.17.0 // indirect
