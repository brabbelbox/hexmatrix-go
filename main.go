package main

import (
	"flag"
	"fmt"
	"os"
	"os/signal"
	"syscall"
	"time"

	"github.com/xthexder/go-jack"
)

var gainMatrix = [OUT_PORTS][IN_PORTS]float32{}

// Map of all output to an array of all inputs relevant for this output
// to optimizing the process loop
var channelMap = [OUT_PORTS][]int{}

func updateChannelMap() {
	for outIndex := 0; outIndex < OUT_PORTS; outIndex++ {
		channelMap[outIndex] = make([]int, 0)
		for inIndex := 0; inIndex < IN_PORTS; inIndex++ {
			if gainMatrix[outIndex][inIndex] != 0 {
				channelMap[outIndex] = append(channelMap[outIndex], inIndex)
			}
		}
	}
}

type hexmatrix struct {
	client   *jack.Client
	PortsIn  []*jack.Port
	PortsOut []*jack.Port

	inVolumes  []float32
	outVolumes []float32
}

func (v *hexmatrix) Start() error {
	var status int

	// trying to establish JACK client
	for i := 0; i < 1000; i++ {
		v.client, status = jack.ClientOpen("hexmatrix-go", jack.NoStartServer)
		if status == 0 {
			break
		}
	}
	if status != 0 {
		return fmt.Errorf("failed to initialize client, errcode: %d", status)
	}
	defer v.client.Close()

	// registering JACK callback
	if code := v.client.SetProcessCallback(v.process); code != 0 {
		return fmt.Errorf("failed to set process callback: %d", code)
	}
	v.client.OnShutdown(v.shutdown)

	// Activating client
	if code := v.client.Activate(); code != 0 {
		return fmt.Errorf("failed to activate client: %d", code)
	}

	// registering audio channels
	for i := 1; i <= IN_PORTS; i++ {
		portName := fmt.Sprintf("in_%d", i)
		port := v.client.PortRegister(portName, jack.DEFAULT_AUDIO_TYPE, jack.PortIsInput, 0)
		v.PortsIn = append(v.PortsIn, port)
	}
	for i := 1; i <= OUT_PORTS; i++ {
		portName := fmt.Sprintf("out_%d", i)
		port := v.client.PortRegister(portName, jack.DEFAULT_AUDIO_TYPE, jack.PortIsOutput, 0)
		v.PortsOut = append(v.PortsOut, port)
	}

	interrupted := make(chan bool)

	// signal handler
	sigChan := make(chan os.Signal, 2)
	signal.Notify(sigChan, os.Interrupt, syscall.SIGTERM)
	go func() {
		<-sigChan
		v.shutdown()
		interrupted <- true
	}()

	<-interrupted
	return nil
}

// audio process loop
func (v *hexmatrix) process(nframes uint32) int {

	// wait until all ports are registered
	if len(v.PortsIn) < IN_PORTS || len(v.PortsOut) < OUT_PORTS {
		return 1
	}

	// get all in buffers and their volumes
	inBuffer := [IN_PORTS][]jack.AudioSample{}
	for i := 0; i < IN_PORTS; i++ {
		inBuffer[i] = v.PortsIn[i].GetBuffer(nframes)
		v.inVolumes[i] = max(float32(getHighestSpread(inBuffer[i])), v.inVolumes[i])
	}

	// for each output mix their inputs
	for outIndex, out := range v.PortsOut {

		// no inputs?
		if len(channelMap[outIndex]) == 0 {
			// shortcut for better performance
			v.outVolumes[outIndex] = 0
			continue
		}

		samplesOut := out.GetBuffer(nframes)

		// mix samples
		for sampleIndex, _ := range samplesOut {
			sample := float32(0)
			for _, inIndex := range channelMap[outIndex] {
				sample += float32(inBuffer[inIndex][sampleIndex]) * gainMatrix[outIndex][inIndex]
			}
			samplesOut[sampleIndex] = jack.AudioSample(sample)
		}

		// get output volume
		v.outVolumes[outIndex] = max(float32(getHighestSpread(samplesOut)), v.outVolumes[outIndex])
	}
	return 0
}

func (v *hexmatrix) shutdown() {
	fmt.Print("closing jack connection...")
	v.client.Close()
}

func getHighestSpread(samples []jack.AudioSample) jack.AudioSample {
	var winner jack.AudioSample
	for _, s := range samples {
		if s < 0 {
			s = -s
		}

		if s > winner {
			winner = s
		}
	}
	return winner
}

func meterReduceJob(v *hexmatrix) {
	for {
		time.Sleep(100 * time.Millisecond)
		for i := 0; i < IN_PORTS; i++ {
			v.inVolumes[i] = max(v.inVolumes[i]*0.8-0.05, 0)
		}
		for i := 0; i < OUT_PORTS; i++ {
			v.outVolumes[i] = max(v.outVolumes[i]*0.8-0.05, 0)
		}
	}
}

func main() {
	flagListen := flag.String("listen", "127.0.0.1:7712", "listening address for the HTTP Server")
	flag.Parse()

	hexmatrix := hexmatrix{
		nil,
		[]*jack.Port{},
		[]*jack.Port{},
		make([]float32, IN_PORTS),
		make([]float32, OUT_PORTS),
	}

	go startHttpServer(&hexmatrix, flagListen)
	go meterReduceJob(&hexmatrix)

	err := hexmatrix.Start()
	if err != nil {
		panic(err)
	}
	fmt.Println("Bye!")
}
