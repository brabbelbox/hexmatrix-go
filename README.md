# hexmatrix-go

rewrite of [brabbelbox/hexmatrix](https://gitlab.com/brabbelbox/hexmatrix) in go.

![screenshot of jack patchbay with the hexmatrix block connected to multiple inputs and outputs](images/catia.jpg)

## Usage

### Start
```
pw-jack go run .
```

### Endpoints
- GET `/matrix`
- POST `/matrix`
- WebSocket `/ws`
- GET `/health`

### `/matrix` data structure
`float[out][in]` <- for each output there is an array of gains per input which should be mixed together for this output


