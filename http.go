package main

import (
	"encoding/json"
	"fmt"
	"io"
	"log"
	"net/http"
	"time"

	"github.com/gorilla/websocket"
)

var upgrader = websocket.Upgrader{
	CheckOrigin: func(r *http.Request) bool { return true },
}

func startHttpServer(v *hexmatrix, listenAddr *string) {

	http.HandleFunc("/ws", func(w http.ResponseWriter, r *http.Request) {
		fmt.Println(r.Method, r.URL)
		// Upgrade upgrades the HTTP server connection to the WebSocket protocol.
		conn, err := upgrader.Upgrade(w, r, nil)
		if err != nil {
			log.Print("upgrade failed: ", err)
			return
		}
		defer conn.Close()

		for {
			data := map[string]map[string]interface{}{
				"meters": {
					"in":  v.inVolumes,
					"out": v.outVolumes,
				},
			}
			out, err := json.Marshal(data)
			if err != nil {
				panic(err)
			}
			err2 := conn.WriteMessage(websocket.TextMessage, []byte(out))
			if err2 != nil {
				log.Println("write failed:", err2)
				break
			}
			time.Sleep(100 * time.Millisecond)
		}
	})

	http.HandleFunc("/matrix", func(w http.ResponseWriter, r *http.Request) {
		fmt.Println(r.Method, r.URL)
		if r.Method == "POST" {
			decoder := json.NewDecoder(r.Body)
			var t [][]float32
			err := decoder.Decode(&t)
			if err != nil {
				log.Println(err)
			} else {
				// set all gains to zero in case some are not
				// provided in the request body
				for i := 0; i < OUT_PORTS; i++ {
					for j := 0; j < IN_PORTS; j++ {
						gainMatrix[i][j] = 0
					}
				}

				// apply gains
				for outIndex, gains := range t {
					for inIndex, gain := range gains {
						gainMatrix[outIndex][inIndex] = gain
					}
				}
				updateChannelMap()

				fmt.Println("applied matrix update")
			}
		} else {
			out, err := json.Marshal(gainMatrix)
			if err != nil {
				panic(err)
			}
			w.Header().Set("Content-Type", "application/json")
			io.WriteString(w, string(out))
		}
	})

	http.HandleFunc("/health", func(w http.ResponseWriter, r *http.Request) {
		fmt.Println(r.Method, r.URL)
		inConnections := 0
		outConnections := 0
		for _, port := range v.PortsIn {
			if len(port.GetConnections()) > 0 {
				inConnections++
			}
		}
		for _, port := range v.PortsOut {
			if len(port.GetConnections()) > 0 {
				outConnections++
			}
		}
		data := map[string]interface{}{
			"in_connections":  inConnections,
			"out_connections": outConnections,
		}
		out, err := json.Marshal(data)
		if err != nil {
			panic(err)
		}
		w.Header().Set("Content-Type", "application/json")
		io.WriteString(w, string(out))
	})

	fmt.Println("Start HTTP server on http://" + *listenAddr)
	http.ListenAndServe(*listenAddr, nil)
}
